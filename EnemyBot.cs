﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SeaWars
{
    public class EnemyBot
    {
        public int[,] myMap = new int[Form1.MapSize, Form1.MapSize];//bot`s map
        public int[,] enemyMap = new int[Form1.MapSize, Form1.MapSize];//player`s map

        public Button[,] myButtons = new Button[Form1.MapSize, Form1.MapSize];
        public Button[,] enemyButtons = new Button[Form1.MapSize, Form1.MapSize];

        public EnemyBot(int[,] myMap,int[,] enemyMap,Button[,] myButtons,Button[,] enemyButtons)
        {
            this.myMap = myMap;
            this.enemyMap = enemyMap;
            this.enemyButtons = enemyButtons;
            this.myButtons = myButtons;
        }
        public EnemyBot(EnemyBot obj)
        {
            this.myMap = obj.myMap;
            this.enemyMap = obj.enemyMap;
            this.enemyButtons = obj.enemyButtons;
            this.myButtons = obj.myButtons;
        }

        public bool IsInsideMap(int i,int j)
        {
            if(i<0 || j<0 || i>= Form1.MapSize || j>= Form1.MapSize)
            {
                return false;
            }
            return true;
        }

        public bool Empty(int i,int j,int length)
        {
            bool isEmpty = true;

            for (int k = j; k < j + length; k++)
            {
                if (myMap[i, k] != 0)
                {
                    isEmpty = false;
                    break;
                }
            }
            return isEmpty;
        }

        public int[,] PlacementOfShips()
        {

            int lengthShip = 4;
            int cycleValue = 4;
            int shipsCount = 10;
            Random r = new Random();

            int posX = 0;
            int posY = 0;


            while (shipsCount > 0)
            {
                for (int i = 0; i < cycleValue / 4; i++)
                {
                    posX = r.Next(1, Form1.MapSize);
                    posY = r.Next(1, Form1.MapSize);

                    while (!IsInsideMap(posX + 1, posY + lengthShip + 1) || !Empty(posX + 1, posY - 1, lengthShip + 2) ||
                        !IsInsideMap(posX - 1, posY + lengthShip + 1) || !Empty(posX - 1, posY - 1, lengthShip + 2) ||
                        !IsInsideMap(posX, posY + lengthShip + 1) || !Empty(posX, posY - 1, lengthShip + 2))
                    {
                        posX = r.Next(1, Form1.MapSize);
                        posY = r.Next(1, Form1.MapSize);
                    }
                    for (int k = posY; k < posY + lengthShip; k++)
                    {
                        myMap[posX, k] = 1;
                    }
                    shipsCount--;
                    if (shipsCount <= 0)
                        break;
                }
                cycleValue += 4;
                lengthShip--;
            }
            return myMap;
        }
       
        public bool Shoot()
        {
            bool hit = false;
            Random rnd = new Random();

            int posX = rnd.Next(1, Form1.MapSize);
            int posY = rnd.Next(1, Form1.MapSize);

            while (enemyButtons[posX, posY].BackColor == Color.Orange || enemyButtons[posX, posY].BackColor == Color.Blue)
            {
                posX = rnd.Next(1, Form1.MapSize);
                posY = rnd.Next(1, Form1.MapSize);
            }
            if (enemyMap[posX, posY] != 0)
            {
                hit = true;
                enemyMap[posX, posY] = 0;
                enemyButtons[posX, posY].BackColor = Color.Orange;
                enemyButtons[posX, posY].Text = "X";
                Thread.Sleep(100);
            }
            else
            {
                hit = false;
                enemyButtons[posX, posY].BackColor = Color.Blue;
                enemyButtons[posX, posY].Text = ".";
                Thread.Sleep(50);
            }
            if (hit)
                Shoot();
            return hit;
        }
    }
}
