﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
namespace SeaWars
{
    public partial class Form1 : Form
    {
        public const int MapSize = 11;
        public int CellSize = 30;
        public string signature = "РЕСПУБЛІКА";

        public int[,] PlayerMap = new int[MapSize, MapSize];
        public int[,] BotMap = new int[MapSize, MapSize];

        public Button[,] myButtons = new Button[MapSize, MapSize];
        public Button[,] BotButtons = new Button[MapSize, MapSize];

        public bool isPlaying = false;

        public EnemyBot bot;

        public Form1()
        {
            InitializeComponent();            
            Init();
        }
        public void Init()
        {
            isPlaying = false;
            CreateMapsPlayer();
            CreateMapsBot();
            bot = new EnemyBot(BotMap, PlayerMap, BotButtons, myButtons);
            BotMap = bot.PlacementOfShips();
        }
        public void CreateMapsPlayer()
        {
            
            for (int i = 0; i < MapSize; i++)
            {
                for (int j = 0; j < MapSize; j++)
                {
                    PlayerMap[i, j] = 0;

                    Button button = new Button();
                    button.Location = new Point(j * CellSize, i * CellSize);
                    button.Size = new Size(CellSize, CellSize);
                    button.BackColor = Color.White;
                    if (j == 0 || i == 0)
                    {
                        button.BackColor = Color.Gray;
                        if (i == 0 && j > 0)
                            button.Text = signature[j - 1].ToString();
                        if (j == 0 && i > 0)
                            button.Text = i.ToString();
                    }
                    else
                    {
                        button.Click += new EventHandler(PlacementOfShips);
                    }
                    myButtons[i, j] = button;
                    this.Controls.Add(button);
                }
            }
            Label labelPlayer = new Label();
            labelPlayer.Text = "Карта Гравця";
            labelPlayer.Location = new Point((MapSize * CellSize / 2)-30, MapSize * CellSize + 10);
            this.Controls.Add(labelPlayer);

            Label labelBot = new Label();
            labelBot.Text = "Карта Ворога";
            labelBot.Location = new Point(410 + MapSize * CellSize / 2, MapSize * CellSize + 10);
            this.Controls.Add(labelBot);

        }
        public void CreateMapsBot()
        {
            for (int i = 0; i < MapSize; i++)
            {
                for (int j = 0; j < MapSize; j++)
                {
                    PlayerMap[i, j] = 0;
                    BotMap[i, j] = 0;

                    Button button = new Button();
                    button.Location = new Point(440 + j * CellSize, i * CellSize);
                    button.Size = new Size(CellSize, CellSize);
                    button.BackColor = Color.White;
                    if (j == 0 || i == 0)
                    {
                        button.BackColor = Color.Gray;
                        if (i == 0 && j > 0)
                            button.Text = signature[j - 1].ToString();
                        if (j == 0 && i > 0)
                            button.Text = i.ToString();
                    }
                    else
                    {
                        button.Click += new EventHandler(PlayerShoot);
                    }
                    BotButtons[i, j] = button;
                    this.Controls.Add(button);
                }
            }
            Button startButton = new Button();
            startButton.Text = "Почати Гру";
            startButton.Click += new EventHandler(Start);
            startButton.Location = new Point(335, MapSize * CellSize + 10);
            startButton.Size = new Size(110, 50);
            this.Controls.Add(startButton);

            Button regulations = new Button();
            regulations.Text = "Правила гри";
            regulations.Click += new EventHandler(Rules);
            regulations.Location = new Point(15, MapSize * CellSize + 70);
            regulations.Size = new Size(100, 50);
            this.Controls.Add(regulations);

            Button GoOut = new Button();
            GoOut.Text = "Вийти з гри";
            GoOut.Click += new EventHandler(Quit);
            GoOut.Location = new Point(460 + MapSize * CellSize / 2, MapSize * CellSize + 70);
            GoOut.Size = new Size(100, 50);
            this.Controls.Add(GoOut);
        }
        public void Quit(object sender,EventArgs e)
        {
            this.Close();
        }
        public void Start(object sender, EventArgs e)
        {
            isPlaying = true;          
        }
        public void Rules(object sender,EventArgs e)
        {
            MessageBox.Show("«Морський бій »- гра для двох учасників, в якій гравці по черзі називають координати на невідомої їм мапі суперника." +
                "Якщо у суперника по цих координатах є корабель (координати зайняті), то корабель або його частина« топиться », а потрапив отримує право зробити ще один хід." +
                "Мета гравця - першим потопити всі кораблі супротивника.", " Правила  ", MessageBoxButtons.OK, MessageBoxIcon.None);
        }
        public bool CheckEmptyCell()
        {
            bool isEmptyOne = true;
            bool isEmptyTwo = true;
            for (int i = 0; i < MapSize; i++)
            {
                for (int j = 0; j < MapSize; j++)
                {
                    if (PlayerMap[i, j] != 0)
                        isEmptyOne = false;
                    if (BotMap[i, j] !=0)
                        isEmptyTwo = false;
                }
            }
            if (isEmptyOne||isEmptyTwo)
                return false;
            else return true;
        }
       
        public void PlacementOfShips(object sender, EventArgs e)
        {
            Button pressedButton = sender as Button;
            if (!isPlaying)
            {
                if (PlayerMap[pressedButton.Location.Y / CellSize, pressedButton.Location.X / CellSize] == 0)
                {
                    pressedButton.BackColor = Color.Red;
                    PlayerMap[pressedButton.Location.Y / CellSize, pressedButton.Location.X / CellSize] = 1;
                }
                else
                {
                    pressedButton.BackColor = Color.White;
                    PlayerMap[pressedButton.Location.Y / CellSize, pressedButton.Location.X / CellSize] = 0;
                }
            }
        }
        public void PlayerShoot(object sender, EventArgs e)
        {
            Button pressedButton = sender as Button;
            bool playerTurn = Shoot(BotMap, pressedButton);
            if (!playerTurn)
                bot.Shoot();
    
            if (!CheckEmptyCell())
            {
                this.Controls.Clear();
                Init();
                Thread.Sleep(400);
                MessageBox.Show("Гру завершено", "   ", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
        }
        public bool Shoot(int[,] map, Button pressedButton)
        {

            bool hit = false;
            if (isPlaying)
            {
                int delta = 0;
                if (pressedButton.Location.X > 440)
                    delta = 440;
                if (map[pressedButton.Location.Y / CellSize, (pressedButton.Location.X - delta) / CellSize] != 0)
                {
                    hit = true;

                    map[pressedButton.Location.Y / CellSize, (pressedButton.Location.X - delta) / CellSize] = 0;
                    pressedButton.BackColor = Color.Orange;
                    pressedButton.Text = "X";
                    Thread.Sleep(50);
                }
                else
                {
                    hit = false;
                    pressedButton.BackColor = Color.Blue;
                    pressedButton.Text = ".";
                    Thread.Sleep(100);
                }
                if (pressedButton.BackColor != Color.White)
                {
                    pressedButton.Enabled = false;
                }
                else { pressedButton.Enabled = true; }
            }
           if(!isPlaying)
            {
                while(isPlaying)
                pressedButton.Enabled = false;
                MessageBox.Show("Спочатку виставте кораблі", "  ", MessageBoxButtons.OK, MessageBoxIcon.None);
            }
            
            return hit;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
        }
        private void button2_Click(object sender, EventArgs e)
        {
        }
        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
}
